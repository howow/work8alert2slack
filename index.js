'use strict'

const got = require('got')

const SAlert = got.extend({
  baseUrl: 'https://hooks.slack.com/services/',
  heards: {
    'Content-type': 'application/json'
  }
})

// op-notice https://hooks.slack.com/services/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

module.exports = (title, message) => {
  if (message == null) {
    message = title
    title = 'WARNING'
  }

  const msg = (message || '').trim()

  return SAlert
    .post('/T9E2Z1FUK/BNRQSA2VB/MDOzKhgMIlTqvziernwXDE4y', {
      body: JSON.stringify({text: `[${title}] ${msg}`})
    })
    .then(respose => respose)
    .catch(e => e)
}
